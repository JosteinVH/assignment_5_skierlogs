-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 28. Nov, 2017 08:34 AM
-- Server-versjon: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `assignment5`
--

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `club`
--

CREATE TABLE `club` (
  `clubId` varchar(30) NOT NULL,
  `clubName` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `county` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `season`
--

CREATE TABLE `season` (
  `fallYear` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `skier`
--

CREATE TABLE `skier` (
  `userName` varchar(60) NOT NULL,
  `yearOfBirth` varchar(4) NOT NULL,
  `firstName` varchar(60) NOT NULL,
  `lastName` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `skier_distance`
--

CREATE TABLE `skier_distance` (
  `userName` varchar(50) NOT NULL,
  `fallYear` varchar(4) NOT NULL,
  `clubId` varchar(30) DEFAULT NULL,
  `total_distance` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `club`
--
ALTER TABLE `club`
  ADD PRIMARY KEY (`clubId`),
  ADD UNIQUE KEY `clubId` (`clubId`);

--
-- Indexes for table `season`
--
ALTER TABLE `season`
  ADD PRIMARY KEY (`fallYear`),
  ADD UNIQUE KEY `fallYear` (`fallYear`);

--
-- Indexes for table `skier`
--
ALTER TABLE `skier`
  ADD PRIMARY KEY (`userName`),
  ADD UNIQUE KEY `userName` (`userName`);

--
-- Indexes for table `skier_distance`
--
ALTER TABLE `skier_distance`
  ADD PRIMARY KEY (`userName`,`fallYear`),
  ADD KEY `fallYear` (`fallYear`),
  ADD KEY `clubId` (`clubId`);

--
-- Begrensninger for dumpede tabeller
--

--
-- Begrensninger for tabell `skier_distance`
--
ALTER TABLE `skier_distance`
  ADD CONSTRAINT `skier_distance_ibfk_1` FOREIGN KEY (`userName`) REFERENCES `skier` (`userName`),
  ADD CONSTRAINT `skier_distance_ibfk_2` FOREIGN KEY (`fallYear`) REFERENCES `season` (`fallYear`),
  ADD CONSTRAINT `skier_distance_ibfk_3` FOREIGN KEY (`clubId`) REFERENCES `club` (`clubId`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
