<?php
include_once("Model/Model.php");

class Controller {
	public $model;

	public function __construct()
    {
        $this->model = new Model();
    }

/** The one function running the controller code.
 */
	public function invoke()
	{
    $this->model->parseXML();
  }


}
?>
