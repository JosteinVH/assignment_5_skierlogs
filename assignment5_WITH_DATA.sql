-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 28. Nov, 2017 09:06 AM
-- Server-versjon: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `assignment5`
--

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `club`
--

CREATE TABLE `club` (
  `clubId` varchar(30) NOT NULL,
  `clubName` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `county` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dataark for tabell `club`
--

INSERT INTO `club` (`clubId`, `clubName`, `city`, `county`) VALUES
('asker-ski', 'Asker skiklubb', 'Akershus', 'Akershus'),
('lhmr-ski', 'Lillehammer Skiklub', 'Oppland', 'Oppland'),
('skiklubben', 'Trondhjems skiklub', 'Sør-Trøndelag', 'Sør-Trøndelag'),
('vindil', 'Vind Idrettslag', 'Oppland', 'Oppland');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `season`
--

CREATE TABLE `season` (
  `fallYear` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dataark for tabell `season`
--

INSERT INTO `season` (`fallYear`) VALUES
('2015'),
('2016');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `skier`
--

CREATE TABLE `skier` (
  `userName` varchar(60) NOT NULL,
  `yearOfBirth` varchar(4) NOT NULL,
  `firstName` varchar(60) NOT NULL,
  `lastName` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dataark for tabell `skier`
--

INSERT INTO `skier` (`userName`, `yearOfBirth`, `firstName`, `lastName`) VALUES
('?hal_?mos', '2009', '?Halvor', '?Mostuen'),
('?jan_tang', '2007', '?Jan', 'Tangen'),
('?rut_?mos', '2002', '?Ruth', '?Mostuen'),
('?rut_nord', '2006', '?Ruth', 'Nordli'),
('ande_andr', '2004', 'Anders', 'Andresen'),
('ande_rønn', '2001', 'Anders', 'Rønning'),
('andr_stee', '2001', 'Andreas', 'Steen'),
('anna_næss', '2005', 'Anna', 'Næss'),
('arne_anto', '2005', 'Arne', 'Antonsen'),
('arne_inge', '2005', 'Arne', 'Ingebrigtsen'),
('astr_amun', '2001', 'Astrid', 'Amundsen'),
('astr_sven', '2008', 'Astrid', 'Svendsen'),
('bent_håla', '2009', 'Bente', 'Håland'),
('bent_svee', '2003', 'Bente', 'Sveen'),
('beri_hans', '2003', 'Berit', 'Hanssen'),
('bjør_aase', '2006', 'Bjørn', 'Aasen'),
('bjør_ali', '2008', 'Bjørn', 'Ali'),
('bjør_rønn', '2009', 'Bjørg', 'Rønningen'),
('bjør_sand', '1997', 'Bjørn', 'Sandvik'),
('bror_?mos', '2005', 'Bror', '?Mostuen'),
('bror_kals', '2006', 'Bror', 'Kalstad'),
('cami_erik', '2005', 'Camilla', 'Eriksen'),
('dani_hamm', '2000', 'Daniel', 'Hammer'),
('eina_nygå', '2009', 'Einar', 'Nygård'),
('elis_ruud', '2003', 'Elisabeth', 'Ruud'),
('elle_wiik', '2004', 'Ellen', 'Wiik'),
('erik_haal', '2007', 'Erik', 'Haaland'),
('erik_lien', '2008', 'Erik', 'Lien'),
('erik_pete', '2002', 'Erik', 'Petersen'),
('espe_egel', '2005', 'Espen', 'Egeland'),
('espe_haal', '2004', 'Espen', 'Haaland'),
('eva_kvam', '2000', 'Eva', 'Kvam'),
('fred_lien', '2000', 'Fredrik', 'Lien'),
('frod_mads', '2008', 'Frode', 'Madsen'),
('frod_rønn', '2005', 'Frode', 'Rønningen'),
('geir_birk', '2010', 'Geir', 'Birkeland'),
('geir_herm', '2003', 'Geir', 'Hermansen'),
('gerd_svee', '2001', 'Gerd', 'Sveen'),
('gunn_berg', '2009', 'Gunnar', 'Berge'),
('guri_nord', '2003', 'Guri', 'Nordli'),
('hann_stei', '2005', 'Hanno', 'Steiro'),
('hans_foss', '1998', 'Hans', 'Foss'),
('hans_løke', '2005', 'Hans', 'Løken'),
('hara_bakk', '2002', 'Harald', 'Bakken'),
('heid_dani', '2005', 'Heidi', 'Danielsen'),
('helg_brei', '2006', 'Helge', 'Breivik'),
('helg_toll', '2003', 'Helge', 'Tollefsen'),
('henr_bern', '2003', 'Henrik', 'Berntsen'),
('henr_dale', '2005', 'Henrik', 'Dalen'),
('henr_lore', '2006', 'Henrik', 'Lorentzen'),
('hild_hass', '2007', 'Hilde', 'Hassan'),
('håko_jens', '2005', 'Håkon', 'Jensen'),
('idar_kals', '2007', 'Idar', 'Kalstad'),
('idar_kals1', '2002', 'Idar', 'Kalstad'),
('ida_mykl', '2001', 'Ida', 'Myklebust'),
('inge_simo', '2004', 'Inger', 'Simonsen'),
('inge_thor', '2006', 'Inger', 'Thorsen'),
('ingr_edva', '2001', 'Ingrid', 'Edvardsen'),
('juli_ande', '2003', 'Julie', 'Andersson'),
('kari_thor', '2002', 'Karin', 'Thorsen'),
('kjel_fjel', '2004', 'Kjell', 'Fjeld'),
('knut_bye', '2006', 'Knut', 'Bye'),
('kris_even', '2004', 'Kristian', 'Evensen'),
('kris_hass', '2003', 'Kristin', 'Hassan'),
('kris_hass1', '2004', 'Kristian', 'Hassan'),
('lind_lore', '2004', 'Linda', 'Lorentzen'),
('liv_khan', '2006', 'Liv', 'Khan'),
('magn_sand', '2003', 'Magnus', 'Sande'),
('mari_bye', '2003', 'Marit', 'Bye'),
('mari_dahl', '2004', 'Marit', 'Dahl'),
('mari_eile', '2000', 'Marius', 'Eilertsen'),
('mari_stra', '2005', 'Marius', 'Strand'),
('mart_halv', '2002', 'Martin', 'Halvorsen'),
('mona_lie', '2004', 'Mona', 'Lie'),
('mort_iver', '2003', 'Morten', 'Iversen'),
('nils_bakk', '2003', 'Nils', 'Bakke'),
('nils_knud', '2006', 'Nils', 'Knudsen'),
('odd_moha', '2005', 'Odd', 'Mohamed'),
('olav_bråt', '2000', 'Olav', 'Bråthen'),
('olav_eike', '2008', 'Olav', 'Eikeland'),
('olav_hell', '2007', 'Olav', 'Helle'),
('olav_lien', '2002', 'Olav', 'Lien'),
('ole_borg', '2002', 'Ole', 'Borge'),
('reid_hamr', '2008', 'Reidun', 'Hamre'),
('rolf_wiik', '2002', 'Rolf', 'Wiik'),
('rune_haga', '2005', 'Rune', 'Haga'),
('sara_okst', '2003', 'Sarah', 'Okstad'),
('silj_mykl', '2007', 'Silje', 'Myklebust'),
('sive_nord', '2009', 'Sivert', 'Nordli'),
('solv_solb', '2004', 'Solveig', 'Solbakken'),
('stia_andr', '2004', 'Stian', 'Andreassen'),
('stia_haug', '2002', 'Stian', 'Haugland'),
('stia_henr', '2001', 'Stian', 'Henriksen'),
('terj_mort', '2003', 'Terje', 'Mortensen'),
('thom_inge', '2006', 'Thomas', 'Ingebrigtsen'),
('tom_bråt', '2008', 'Tom', 'Bråthen'),
('tom_bøe', '2008', 'Tom', 'Bøe'),
('tom_jako', '2002', 'Tom', 'Jakobsen'),
('tore_gulb', '2005', 'Tore', 'Gulbrandsen'),
('tore_svee', '2001', 'Tore', 'Sveen'),
('tor_dale', '2005', 'Tor', 'Dalen'),
('tove_moe', '2002', 'Tove', 'Moe'),
('trin_kals', '2009', 'Trine', 'Kalstad'),
('tron_kris', '2006', 'Trond', 'Kristensen'),
('tron_moen', '2004', 'Trond', 'Moen'),
('øyst_aase', '2007', 'Øystein', 'Aasen'),
('øyst_lore', '2004', 'Øystein', 'Lorentzen'),
('øyst_sæth', '2000', 'Øystein', 'Sæther'),
('øyvi_hell', '2000', 'Øyvind', 'Helle'),
('øyvi_jens', '1999', 'Øyvind', 'Jenssen'),
('øyvi_kvam', '2000', 'Øyvind', 'Kvam'),
('øyvi_vike', '2004', 'Øyvind', 'Viken');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `skier_distance`
--

CREATE TABLE `skier_distance` (
  `userName` varchar(50) NOT NULL,
  `fallYear` varchar(4) NOT NULL,
  `clubId` varchar(30) DEFAULT NULL,
  `total_distance` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dataark for tabell `skier_distance`
--

INSERT INTO `skier_distance` (`userName`, `fallYear`, `clubId`, `total_distance`) VALUES
('?hal_?mos', '2016', 'asker-ski', '3'),
('?jan_tang', '2015', NULL, '2'),
('?jan_tang', '2016', NULL, '4'),
('?rut_?mos', '2016', 'vindil', '1237'),
('?rut_nord', '2016', 'skiklubben', '368'),
('ande_andr', '2015', 'skiklubben', '23'),
('ande_andr', '2016', 'skiklubben', '55'),
('ande_rønn', '2015', 'lhmr-ski', '942'),
('andr_stee', '2015', 'asker-ski', '440'),
('andr_stee', '2016', 'asker-ski', '379'),
('anna_næss', '2015', 'skiklubben', '3'),
('anna_næss', '2016', 'skiklubben', '3'),
('arne_anto', '2015', 'skiklubben', '32'),
('arne_anto', '2016', 'skiklubben', '99'),
('arne_inge', '2015', 'skiklubben', '1'),
('arne_inge', '2016', 'skiklubben', '2'),
('astr_amun', '2015', 'lhmr-ski', '961'),
('astr_amun', '2016', 'lhmr-ski', '761'),
('astr_sven', '2015', 'skiklubben', '1'),
('astr_sven', '2016', 'skiklubben', '3'),
('bent_håla', '2015', 'asker-ski', '19'),
('bent_håla', '2016', 'skiklubben', '62'),
('bent_svee', '2015', 'asker-ski', '125'),
('beri_hans', '2015', 'asker-ski', '448'),
('beri_hans', '2016', 'asker-ski', '374'),
('bjør_aase', '2015', 'asker-ski', '121'),
('bjør_aase', '2016', 'asker-ski', '116'),
('bjør_ali', '2015', 'asker-ski', '47'),
('bjør_ali', '2016', 'asker-ski', '47'),
('bjør_rønn', '2015', 'lhmr-ski', '33'),
('bjør_rønn', '2016', 'lhmr-ski', '56'),
('bjør_sand', '2015', 'lhmr-ski', '460'),
('bjør_sand', '2016', 'lhmr-ski', '449'),
('bror_?mos', '2016', 'skiklubben', '243'),
('bror_kals', '2016', NULL, '202'),
('cami_erik', '2015', 'vindil', '1'),
('cami_erik', '2016', 'vindil', '1'),
('dani_hamm', '2015', 'lhmr-ski', '33'),
('dani_hamm', '2016', 'lhmr-ski', '61'),
('eina_nygå', '2015', 'lhmr-ski', '31'),
('eina_nygå', '2016', 'skiklubben', '68'),
('elis_ruud', '2015', 'asker-ski', '341'),
('elis_ruud', '2016', 'skiklubben', '368'),
('elle_wiik', '2015', 'lhmr-ski', '12'),
('elle_wiik', '2016', 'lhmr-ski', '35'),
('erik_haal', '2015', 'lhmr-ski', '122'),
('erik_haal', '2016', 'lhmr-ski', '143'),
('erik_lien', '2015', 'vindil', '1'),
('erik_pete', '2015', 'vindil', '581'),
('espe_egel', '2015', 'skiklubben', '519'),
('espe_egel', '2016', 'skiklubben', '556'),
('espe_haal', '2015', 'lhmr-ski', '1'),
('espe_haal', '2016', 'lhmr-ski', '2'),
('eva_kvam', '2015', 'skiklubben', '28'),
('eva_kvam', '2016', 'skiklubben', '89'),
('fred_lien', '2015', 'asker-ski', '113'),
('fred_lien', '2016', 'asker-ski', '122'),
('frod_mads', '2015', 'skiklubben', '1'),
('frod_mads', '2016', 'skiklubben', '2'),
('frod_rønn', '2015', 'lhmr-ski', '237'),
('geir_birk', '2015', 'skiklubben', '69'),
('geir_birk', '2016', 'skiklubben', '71'),
('geir_herm', '2015', 'asker-ski', '891'),
('geir_herm', '2016', 'skiklubben', '789'),
('gerd_svee', '2015', 'lhmr-ski', '173'),
('gerd_svee', '2016', 'lhmr-ski', '196'),
('gunn_berg', '2015', 'vindil', '2'),
('gunn_berg', '2016', 'vindil', '2'),
('guri_nord', '2016', 'skiklubben', '17'),
('hann_stei', '2016', 'lhmr-ski', '14'),
('hans_foss', '2015', 'asker-ski', '240'),
('hans_foss', '2016', 'lhmr-ski', '276'),
('hans_løke', '2015', 'skiklubben', '3'),
('hans_løke', '2016', 'skiklubben', '1'),
('hara_bakk', '2015', 'lhmr-ski', '7'),
('hara_bakk', '2016', 'lhmr-ski', '16'),
('heid_dani', '2015', 'asker-ski', '3'),
('heid_dani', '2016', 'asker-ski', '3'),
('helg_brei', '2015', 'skiklubben', '27'),
('helg_brei', '2016', 'skiklubben', '74'),
('helg_toll', '2015', 'skiklubben', '9'),
('henr_bern', '2015', 'asker-ski', '799'),
('henr_dale', '2015', NULL, '2'),
('henr_dale', '2016', NULL, '2'),
('henr_lore', '2015', 'vindil', '1'),
('henr_lore', '2016', 'vindil', '1'),
('hild_hass', '2015', 'lhmr-ski', '2'),
('hild_hass', '2016', 'lhmr-ski', '1'),
('håko_jens', '2015', 'lhmr-ski', '778'),
('håko_jens', '2016', 'lhmr-ski', '804'),
('idar_kals', '2016', 'skiklubben', '101'),
('idar_kals1', '2016', 'vindil', '1308'),
('ida_mykl', '2015', 'skiklubben', '666'),
('ida_mykl', '2016', 'skiklubben', '614'),
('inge_simo', '2015', 'asker-ski', '3'),
('inge_simo', '2016', 'asker-ski', '2'),
('inge_thor', '2015', NULL, '194'),
('inge_thor', '2016', NULL, '220'),
('ingr_edva', '2015', 'skiklubben', '294'),
('ingr_edva', '2016', 'skiklubben', '309'),
('juli_ande', '2015', 'skiklubben', '20'),
('juli_ande', '2016', 'skiklubben', '34'),
('kari_thor', '2015', 'skiklubben', '261'),
('kari_thor', '2016', 'skiklubben', '233'),
('kjel_fjel', '2015', 'asker-ski', '1'),
('kjel_fjel', '2016', 'skiklubben', '2'),
('knut_bye', '2015', 'lhmr-ski', '2'),
('kris_even', '2015', 'skiklubben', '586'),
('kris_hass', '2015', 'skiklubben', '4'),
('kris_hass', '2016', 'skiklubben', '11'),
('kris_hass1', '2015', 'skiklubben', '391'),
('kris_hass1', '2016', 'lhmr-ski', '334'),
('lind_lore', '2015', 'lhmr-ski', '578'),
('lind_lore', '2016', 'lhmr-ski', '551'),
('liv_khan', '2015', 'asker-ski', '178'),
('liv_khan', '2016', 'asker-ski', '183'),
('magn_sand', '2015', 'asker-ski', '200'),
('magn_sand', '2016', 'asker-ski', '166'),
('mari_bye', '2015', NULL, '362'),
('mari_dahl', '2015', 'lhmr-ski', '576'),
('mari_dahl', '2016', 'lhmr-ski', '492'),
('mari_eile', '2015', 'lhmr-ski', '18'),
('mari_eile', '2016', 'lhmr-ski', '18'),
('mari_stra', '2015', 'skiklubben', '41'),
('mari_stra', '2016', 'skiklubben', '35'),
('mart_halv', '2015', 'vindil', '63'),
('mart_halv', '2016', 'vindil', '50'),
('mona_lie', '2015', 'skiklubben', '7'),
('mona_lie', '2016', 'skiklubben', '12'),
('mort_iver', '2015', 'skiklubben', '2'),
('mort_iver', '2016', 'skiklubben', '4'),
('nils_bakk', '2015', 'lhmr-ski', '36'),
('nils_bakk', '2016', 'lhmr-ski', '93'),
('nils_knud', '2015', 'skiklubben', '4'),
('nils_knud', '2016', 'skiklubben', '2'),
('odd_moha', '2015', 'skiklubben', '352'),
('olav_bråt', '2015', NULL, '17'),
('olav_bråt', '2016', NULL, '19'),
('olav_eike', '2015', 'lhmr-ski', '2'),
('olav_eike', '2016', 'lhmr-ski', '2'),
('olav_hell', '2015', 'skiklubben', '1'),
('olav_lien', '2015', 'asker-ski', '408'),
('olav_lien', '2016', 'asker-ski', '423'),
('ole_borg', '2015', 'lhmr-ski', '311'),
('ole_borg', '2016', 'lhmr-ski', '314'),
('reid_hamr', '2015', 'skiklubben', '2'),
('reid_hamr', '2016', 'skiklubben', '3'),
('rolf_wiik', '2015', 'skiklubben', '749'),
('rolf_wiik', '2016', 'skiklubben', '632'),
('rune_haga', '2015', 'asker-ski', '228'),
('rune_haga', '2016', 'asker-ski', '248'),
('sara_okst', '2016', 'asker-ski', '5'),
('silj_mykl', '2015', 'asker-ski', '1'),
('silj_mykl', '2016', 'asker-ski', '2'),
('sive_nord', '2016', 'skiklubben', '1'),
('solv_solb', '2015', NULL, '2'),
('solv_solb', '2016', 'asker-ski', '1'),
('stia_andr', '2015', NULL, '8'),
('stia_andr', '2016', 'vindil', '9'),
('stia_haug', '2015', 'skiklubben', '412'),
('stia_haug', '2016', 'skiklubben', '443'),
('stia_henr', '2015', 'vindil', '62'),
('stia_henr', '2016', 'vindil', '49'),
('terj_mort', '2015', 'skiklubben', '119'),
('terj_mort', '2016', 'skiklubben', '95'),
('thom_inge', '2015', 'vindil', '15'),
('thom_inge', '2016', 'vindil', '26'),
('tom_bråt', '2015', 'vindil', '1'),
('tom_bråt', '2016', 'vindil', '1'),
('tom_bøe', '2015', 'vindil', '176'),
('tom_bøe', '2016', 'vindil', '194'),
('tom_jako', '2015', 'asker-ski', '18'),
('tom_jako', '2016', 'skiklubben', '33'),
('tore_gulb', '2015', 'lhmr-ski', '375'),
('tore_gulb', '2016', 'lhmr-ski', '342'),
('tore_svee', '2015', 'skiklubben', '1156'),
('tor_dale', '2015', 'skiklubben', '408'),
('tove_moe', '2015', 'asker-ski', '321'),
('tove_moe', '2016', 'asker-ski', '352'),
('trin_kals', '2016', 'lhmr-ski', '22'),
('tron_kris', '2015', 'skiklubben', '3'),
('tron_kris', '2016', 'skiklubben', '5'),
('tron_moen', '2015', 'vindil', '8'),
('tron_moen', '2016', 'vindil', '17'),
('øyst_aase', '2015', 'skiklubben', '2'),
('øyst_aase', '2016', 'skiklubben', '1'),
('øyst_lore', '2015', 'skiklubben', '13'),
('øyst_lore', '2016', 'skiklubben', '47'),
('øyst_sæth', '2015', 'vindil', '831'),
('øyst_sæth', '2016', 'vindil', '631'),
('øyvi_hell', '2015', 'asker-ski', '950'),
('øyvi_hell', '2016', 'asker-ski', '869'),
('øyvi_jens', '2015', 'skiklubben', '3'),
('øyvi_jens', '2016', 'skiklubben', '2'),
('øyvi_kvam', '2015', 'asker-ski', '18'),
('øyvi_vike', '2015', 'asker-ski', '20'),
('øyvi_vike', '2016', 'asker-ski', '52');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `club`
--
ALTER TABLE `club`
  ADD PRIMARY KEY (`clubId`),
  ADD UNIQUE KEY `clubId` (`clubId`);

--
-- Indexes for table `season`
--
ALTER TABLE `season`
  ADD PRIMARY KEY (`fallYear`),
  ADD UNIQUE KEY `fallYear` (`fallYear`);

--
-- Indexes for table `skier`
--
ALTER TABLE `skier`
  ADD PRIMARY KEY (`userName`),
  ADD UNIQUE KEY `userName` (`userName`);

--
-- Indexes for table `skier_distance`
--
ALTER TABLE `skier_distance`
  ADD PRIMARY KEY (`userName`,`fallYear`),
  ADD KEY `fallYear` (`fallYear`),
  ADD KEY `clubId` (`clubId`);

--
-- Begrensninger for dumpede tabeller
--

--
-- Begrensninger for tabell `skier_distance`
--
ALTER TABLE `skier_distance`
  ADD CONSTRAINT `skier_distance_ibfk_1` FOREIGN KEY (`userName`) REFERENCES `skier` (`userName`),
  ADD CONSTRAINT `skier_distance_ibfk_2` FOREIGN KEY (`fallYear`) REFERENCES `season` (`fallYear`),
  ADD CONSTRAINT `skier_distance_ibfk_3` FOREIGN KEY (`clubId`) REFERENCES `club` (`clubId`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
