<?php

class Model {
  protected $db, $xmlDoc;

  function __construct()
  {
    $this->xmlDoc = new DOMDocument();
    $this->xmlDoc->load('Model/SkierLogs.xml');
            // Create PDO connection
    try{
      $this->db = new PDO('mysql:dbname=assignment5;host=localhost;charset=utf8', 'root', '');
      $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      //Error reporting   //Throw exception
    }
    catch(PDOException $e){
      echo "Error!: " . $e->getMessage() . '<br>';
    }
 }

  public function parseXML(){
    $this->clubparse();
    $this->skierParse();
    $this->seasonParse();
    $this->skierdistanceParse();
  }

  public function clubparse() {
    $club = $this->xmlDoc->getElementsByTagName("Club");
    $clubN = $this->xmlDoc->getElementsByTagName("Name");
    $clubC = $this->xmlDoc->getElementsByTagName("City");
    $clubC = $this->xmlDoc->getElementsByTagName("County");
    $clubId = array();
    $clubName = array();
    $clubCity = array();
    $clubCounty = array();
    $sesong = array();

    foreach ($club as $valueId) {
      array_push($clubId, $valueId->getAttribute("id"));
    }

    foreach ($clubN as $valueName) {
      array_push($clubName,$valueName->nodeValue);
    }
    foreach ($clubC as $valueCity) {
      array_push($clubCity,$valueCity->nodeValue);
    }

    foreach ($clubC as $valueCounty){
      array_push($clubCounty, $valueCounty->nodeValue);
    }
    for ($i=0; $i <sizeof($clubId); $i++){
        try{
          $stmt = $this->db->prepare('INSERT INTO club(clubId,clubName,city,county) VALUES(:id,:name,:ci,:cou)');
          $stmt->bindValue(':id',$clubId[$i],PDO::PARAM_STR);
          $stmt->bindValue(':name',$clubName[$i],PDO::PARAM_STR);
          $stmt->bindValue(':ci',$clubCity[$i],PDO::PARAM_STR);
          $stmt->bindValue(':cou',$clubCounty[$i],PDO::PARAM_STR);
          $stmt->execute();
          }

            catch(PDOException $e) {
              $e->getMessage();
          }
          echo "Clubs Success <br>";
        }
  }

  public function skierParse() {
    $yob = $this->xmlDoc->getElementsByTagName("YearOfBirth");
    $fN = $this->xmlDoc->getElementsByTagName("FirstName");
    $lstN = $this->xmlDoc->getElementsByTagName("LastName");
    $skierId = array();
    $skierFn = array();
    $skierLn = array();
    $skierYob = array();

    $xpath = new DOMXPath($this->xmlDoc);
    $query = '//SkierLogs/Skiers/Skier';
    $entries = $xpath->query($query);
    foreach($entries as $sId){
      array_push($skierId,$sId->getAttribute("userName"));
    }

    foreach ($yob as $y) {
      array_push($skierYob,$y->nodeValue);
    }
    foreach ($fN as $f) {
      array_push($skierFn,$f->nodeValue);
    }
    foreach ($lstN as $l) {
      array_push($skierLn,$l->nodeValue);
    }

    for ($i=0; $i < sizeOf($skierId); $i++) {
      try{
        $stmt = $this->db->prepare('INSERT INTO skier(userName,yearOfBirth,firstName,lastName) VALUES(:id,:yob,:fn,:lastN)');
        $stmt->bindValue(':id',$skierId[$i],PDO::PARAM_STR);
        $stmt->bindValue(':yob',$skierYob[$i],PDO::PARAM_STR);
        $stmt->bindValue(':fn',$skierFn[$i],PDO::PARAM_STR);
        $stmt->bindValue(':lastN',$skierLn[$i],PDO::PARAM_STR);
        $stmt->execute();
        echo "Skiers Success";
      }

      catch(PDOException $e) {
        echo "Error";  $e->getMessage();
      }
    }
  }

  public function seasonParse(){
    $season = $this->xmlDoc->getElementsByTagName("Season");
    $s = array();
    foreach ($season as $sfal) {
      array_push($s,$sfal->getAttribute("fallYear"));
    }

    for ($i=0; $i < sizeOf($s); $i++) {
      try {
        $stmt = $this->db->prepare('INSERT INTO season(fallYear) VALUES(:fallY)');
        $stmt->bindValue(':fallY',$s[$i],PDO::PARAM_STR); //Trodde det var ':'
        $stmt->execute();
        echo "Success";
      }
      catch (Exception $e) {
        echo "Error". $e->getMessage();
      }
    }
  }

  public function skierdistanceParse(){
    $myUserNames = array();
    $user_ses_16 = array(); // For 2016 participants only
    $dist_15 = array();     // Distance for the two seasons
    $dist_16 = array();

    $xpath= new DOMXPath($this->xmlDoc);

    //Usernames who perticipated in 2015 season.

    $userN_15 = $xpath->query('//SkierLogs/Season[@fallYear = "2015"]/Skiers/Skier/@userName');
    foreach($userN_15 as $user){
      array_push($myUserNames, trim($user->textContent));
    }

    //Finding total distance for each username:
    $sum = 0;
    for($i = 0; $i< sizeof($myUserNames); $i++){
      $dist = $xpath->query('//SkierLogs/Season[@fallYear = "2015"]/Skiers/Skier[@userName = "'.$myUserNames[$i].'"]/Log/Entry/Distance');
      for($j = 0; $j <$dist->length; $j++){
        $sum += ($dist->item($j)->textContent);
      }
      array_push($dist_15, $sum);
      $sum = 0;
    }

    //Usernames who perticipated in 2016 season.
    $userN_16 = $xpath->query('//SkierLogs/Season[@fallYear = "2016"]/Skiers/Skier/@userName');
    foreach($userN_16 as $user){
      array_push($user_ses_16, trim($user->textContent));
    }

    $sum = 0;
    for($i = 0; $i< sizeof($user_ses_16); $i++){
      $dist_2 = $xpath->query('//SkierLogs/Season[@fallYear = "2016"]/Skiers/Skier[@userName = "'.$user_ses_16[$i].'"]/Log/Entry/Distance');
      for($j = 0; $j <$dist_2->length; $j++){
        $sum += ($dist_2->item($j)->textContent);
      }
      array_push($dist_16, $sum);
      $sum = 0;
    }



    // Inserting the data from fallYear 2015 in db.
    $ses = $xpath->query('//SkierLogs/Season/@fallYear');

    for($i = 0; $i< sizeof($myUserNames); $i++){
      try {
        $null = NULL;
        $club = $xpath->query('//SkierLogs/Season[@fallYear = "2015"]/Skiers/Skier[@userName = "'.$myUserNames[$i].'"]/ancestor::Skiers/@clubId');
        $input = $this->db->prepare("INSERT INTO skier_distance(userName, fallYear, clubId, total_distance) VALUES (:user, :fall, :club, :tot_dist)");
        $input->bindValue(':user', $myUserNames[$i], PDO::PARAM_STR);
        $input->bindValue(':fall', $ses->item(0)->textContent, PDO::PARAM_STR);
        $input->bindValue(':tot_dist', $dist_15[$i], PDO::PARAM_STR);
        if($club->length){
          $input->bindValue(':club', $club->item(0)->textContent, PDO::PARAM_STR);
        }else $input->bindValue(':club', $null, PDO::PARAM_STR);
        $input->execute();
      } catch (Exception $e) {
        echo "Error".'<br>'; echo $e->getMessage();
      }

    }

    // Inserting the data from fallYear 2016 in db.
    for($i = 0; $i< sizeof($user_ses_16); $i++){
      try {
        $null = NULL;
        $club = $xpath->query('//SkierLogs/Season[@fallYear = "2016"]/Skiers/Skier[@userName = "'.$user_ses_16[$i].'"]/ancestor::Skiers/@clubId');
        $input = $this->db->prepare("INSERT INTO skier_distance(userName, fallYear, clubId, total_distance) VALUES (:user, :fall, :club, :tot_dist)");
        $input->bindValue(':user', $user_ses_16[$i], PDO::PARAM_STR);
        $input->bindValue(':fall', $ses->item(1)->textContent, PDO::PARAM_STR);
        $input->bindValue(':tot_dist', $dist_16[$i], PDO::PARAM_STR);
        if($club->length){
          $input->bindValue(':club', $club->item(0)->textContent, PDO::PARAM_STR);
        }else $input->bindValue(':club', $null, PDO::PARAM_STR);
        $input->execute();
      } catch (Exception $e) {
        echo "Error".'<br>'; echo $e->getMessage();
      }
    }
  }
}


 ?>
